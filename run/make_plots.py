import os
import argparse

parser = argparse.ArgumentParser(description='Produce plots with rootfiles from designated xml',formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-o', '--outfile', action='store',dest='outfile', type=str, nargs='?', default='npa_plots', help="Output directory for the files. Default: %(default)s")
parser.add_argument('-px', '--args.plots_xml', action='store',dest='plots_xml', type=str, nargs='?', default='plots.xml', help="xml file for plots mode. Default: %(default)s")
parser.add_argument('-tx', '--args.tables_xml', action='store',dest='tables_xml', type=str, nargs='?', default='tables.xml', help="xml file for tables mode. Default: %(default)s")
parser.add_argument('-m', '--mode', action='store', dest='mode', type=str, nargs='?', default='plots', 
					help="Mode to be run. Available options:\n"
							"plots - to produce data/bkg plots\n"
							"systshape - to get systematics shape plots\n"
							"tables - to get yields tables\n"
                            "shape - to produce shape plots\n"
							"breakdown - to get systematics breakdown table")
parser.add_argument('-c', '--channel', action='store',dest='channels', type=str, nargs='+', default=["toprare"], help="Array with channel flags to be ran. Default: %(default)s")
parser.add_argument('-f', '--flags', action='store',dest='flags', type=list, nargs='?', default=["","-log"], help="Array with plots flags to be ran. Default: %(default)s")
#parser.add_argument('-r', '--regions', action='store',dest='regions', type=list, nargs='?', default=["all"], help="Array with regions flags to be ran. Default: %(default)s")
parser.add_argument('-sc', '--shape_col', action='store',dest='shape_col', type=str, nargs='?', default="shapes", help="XML collumn for shape plots. Default: %(default)s")
parser.add_argument('-b', '--blind', action='store_true',dest='blind', default=False, help="Run in blinded mode. Default: %(default)s") 
systshape_col = "zjshape"

args = parser.parse_args()
blind = "-blind" if args.blind else ""

def run(exe):
    print "Running command: \n\t {0}".format(exe)
    os.system(exe)

#for region in args.regions:
for channel in args.channels:
#To do plots
	if args.mode in "plots":
            for flag in args.flags:
                run("npa-plots -plots -col {0} -xml {1} -o output/{2}/Plots/{0} {3} {4}".format(channel, args.plots_xml, args.outfile, blind, flag))
                #filename = "sub_{0}_{1}_{2}.sh".format(region,channel,flag)
                #subfile = open(filename,"w+")
                #subfile.write("#PBS -l walltime=70:00:00\n#PBS -l cput=70:00:01\ncd $PBS_O_WORKDIR\n")
                #subfile.write("npa-plots -plots -col {0}_{1} -xml {2} -o output/{3}/{0}/{1} {4} {5}".format(region,channel,args.plots_xml,outfile,blind,flag))
                #subfile.close()
                #os.system("qsub {0} -V".format(filename))
        if args.mode in "systshape":
           run("npa-plots -systshape -col {0} -xml {1} ".format(systshape_col, args.plots_xml))
        if args.mode in "tables":
            run("npa-plots -tab -set {0} -xml {1}".format(channel, args.tables_xml))
        if args.mode in "breakdown":
            run("npa-plots -breakdown -xml {0} -tid {2}_bd".format(channel, args.tables_xml))
if args.mode in "shape":
    for flag in args.flags:
        run("npa-plots -shapes -col {3} -xml {2} -o output/{0}/Shapes/ {1} -blind".format(args.outfile, flag, args.plots_xml, args.shape_col))
        #filename = "sub_shape_{0}.sh".format(flag)
        #subfile = open(filename,"w+")
        #subfile.write("#PBS -l walltime=70:00:00\n#PBS -l cput=70:00:01\ncd $PBS_O_WORKDIR\n")
        #subfile.write("npa-plots -plots -col tt_CR_eeem -xml mc15c_shape.xml -o output/{0}/Shapes/ {1}".format(outfile, flag))
        #subfile.close()
        #os.system("qsub {0} -V".format(filename))
