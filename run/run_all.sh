#!/bin/env bash                                                                                                                                                                                                
#PBS -l walltime=70:00:00
#PBS -l cput=70:00:01
cd $PBS_O_WORKDIR
./RunNPA -sample 1 -output output/ttbar -lepN 2 -syst nominal
