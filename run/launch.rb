#!/usr/bin/env ruby
require "npa"

PREAMBLE = ["#PBS -l walltime=70:00:00","#PBS -l cput=70:00:01","cd $PBS_O_WORKDIR"]

leptonNumber = 3
VERSION = ARGV[0]
analysis_case = 3
mass_S = 50
config=[]

opath = "output/#{VERSION}/"
#opath = "output/#{VERSION}/#{leptonNumber}/"
opt = {
    # "lepN" => "#{leptonNumber}",
    "case" => "#{analysis_case}",
    "mass_S" => "#{mass_S}",
}
  
config += [ 
  {ID: "rareup_first_m20",
   sample: 1,
   output: "#{opath}/rareup_first_m20",
   options: opt
  },
  {ID: "rareup_first_m50",
   sample: 2,
   output: "#{opath}/rareup_first_m50",
   options: opt
  },
  {ID: "rareup_first_m80",
   sample: 3,
   output: "#{opath}/rareup_first_m80",
   options: opt
  },
  {ID: "rareup_first_m90",
   sample: 56,
   output: "#{opath}/rareup_first_m90",
   options: opt
  },
  {ID: "rareup_first_m100",
   sample: 4,
   output: "#{opath}/rareup_first_m100",
   options: opt
  },
  {ID: "rareup_first_m120",
   sample: 5,
   output: "#{opath}/rareup_first_m120",
   options: opt
  },
  {ID: "rareup_first_m150",
   sample: 6,
   output: "#{opath}/rareup_first_m150",
   options: opt
  },
  {ID: "rarecharm_first_m20",
   sample: 7,
   output: "#{opath}/rarecharm_first_m20",
   options: opt
  },
  {ID: "rarecharm_first_m50",
   sample: 8,
   output: "#{opath}/rarecharm_first_m50",
   options: opt
  },
  {ID: "rarecharm_first_m80",
   sample: 9,
   output: "#{opath}/rarecharm_first_m80",
   options: opt
  },
  {ID: "rarecharm_first_m90",
   sample: 57,
   output: "#{opath}/rarecharm_first_m90",
   options: opt
  },
  {ID: "rarecharm_first_m100",
   sample: 10,
   output: "#{opath}/rarecharm_first_m100",
   options: opt
  },
  {ID: "rarecharm_first_m120",
   sample: 11,
   output: "#{opath}/rarecharm_first_m120",
   options: opt
  },
  {ID: "rarecharm_first_m150",
   sample: 12,
   output: "#{opath}/rarecharm_first_m150",
   options: opt
  },
  {ID: "rareup_second_m20",
   sample: 13,
   output: "#{opath}/rareup_second_m20",
   options: opt
  },
  {ID: "rareup_second_m50",
   sample: 14,
   output: "#{opath}/rareup_second_m50",
   options: opt
  },
  {ID: "rareup_second_m80",
   sample: 15,
   output: "#{opath}/rareup_second_m80",
   options: opt
  },
  {ID: "rareup_second_m90",
   sample: 58,
   output: "#{opath}/rareup_second_m90",
   options: opt
  },
  {ID: "rareup_second_m100",
   sample: 16,
   output: "#{opath}/rareup_second_m100",
   options: opt
  },
  {ID: "rareup_second_m120",
   sample: 17,
   output: "#{opath}/rareup_second_m120",
   options: opt
  },
  {ID: "rareup_second_m150",
   sample: 18,
   output: "#{opath}/rareup_second_m150",
   options: opt
  },
  {ID: "rarecharm_second_m20",
   sample: 19,
   output: "#{opath}/rarecharm_second_m20",
   options: opt
  },
  {ID: "rarecharm_second_m50",
   sample: 20,
   output: "#{opath}/rarecharm_second_m50",
   options: opt
  },
  {ID: "rarecharm_second_m80",
   sample: 21,
   output: "#{opath}/rarecharm_second_m80",
   options: opt
  },
  {ID: "rarecharm_second_m90",
   sample: 59,
   output: "#{opath}/rarecharm_second_m90",
   options: opt
  },
  {ID: "rarecharm_second_m100",
   sample: 22,
   output: "#{opath}/rarecharm_second_m100",
   options: opt
  },
  {ID: "rarecharm_second_m120",
   sample: 23,
   output: "#{opath}/rarecharm_second_m120",
   options: opt
  },
  {ID: "rarecharm_second_m150",
   sample: 24,
   output: "#{opath}/rarecharm_second_m150",
   options: opt
  },
  {ID: "rareup_third_m20",
   sample: 25,
   output: "#{opath}/rareup_third_m20",
   options: opt
  },
  {ID: "rareup_third_m30",
   sample: 62,
   output: "#{opath}/rareup_third_m30",
   options: opt
  },
  {ID: "rareup_third_m40",
   sample: 63,
   output: "#{opath}/rareup_third_m40",
   options: opt
  },
  {ID: "rareup_third_m50",
   sample: 26,
   output: "#{opath}/rareup_third_m50",
   options: opt
  },
  {ID: "rareup_third_m60",
   sample: 64,
   output: "#{opath}/rareup_third_m60",
   options: opt
  },
  {ID: "rareup_third_m70",
   sample: 65,
   output: "#{opath}/rareup_third_m70",
   options: opt
  },
  {ID: "rareup_third_m80",
   sample: 27,
   output: "#{opath}/rareup_third_m80",
   options: opt
  },
  {ID: "rareup_third_m90",
   sample: 60,
   output: "#{opath}/rareup_third_m90",
   options: opt
  },
  {ID: "rareup_third_m100",
   sample: 28,
   output: "#{opath}/rareup_third_m100",
   options: opt
  },
  {ID: "rareup_third_m120",
   sample: 29,
   output: "#{opath}/rareup_third_m120",
   options: opt
  },
  {ID: "rareup_third_m150",
   sample: 30,
   output: "#{opath}/rareup_third_m150",
   options: opt
  },
  {ID: "rarecharm_third_m20",
   sample: 31,
   output: "#{opath}/rarecharm_third_m20",
   options: opt
  },
  {ID: "rarecharm_third_m30",
   sample: 66,
   output: "#{opath}/rarecharm_third_m30",
   options: opt
  },
  {ID: "rarecharm_third_m40",
   sample: 67,
   output: "#{opath}/rarecharm_third_m40",
   options: opt
  },
  {ID: "rarecharm_third_m50",
   sample: 32,
   output: "#{opath}/rarecharm_third_m50",
   options: opt
  },
  {ID: "rarecharm_third_m60",
   sample: 68,
   output: "#{opath}/rarecharm_third_m60",
   options: opt
  },
  {ID: "rarecharm_third_m70",
   sample: 69,
   output: "#{opath}/rarecharm_third_m70",
   options: opt
  },
  {ID: "rarecharm_third_m80",
   sample: 33,
   output: "#{opath}/rarecharm_third_m80",
   options: opt
  },
  {ID: "rarecharm_third_m90",
   sample: 61,
   output: "#{opath}/rarecharm_third_m90",
   options: opt
  },
  {ID: "rarecharm_third_m100",
   sample: 34,
   output: "#{opath}/rarecharm_third_m100",
   options: opt
  },
  {ID: "rarecharm_third_m120",
   sample: 35,
   output: "#{opath}/rarecharm_third_m120",
   options: opt
  },
  {ID: "rarecharm_third_m150",
   sample: 36 ,
   output: "#{opath}/rarecharm_third_m150",
   options: opt
  },
  {ID: "tZj",
   sample: 40,
   output: "#{opath}/tZj",
   options: opt
  },
  {ID: "ttbar",
   sample: 41,
   output: "#{opath}/ttbar",
   options: opt
  },
  {ID: "Zjets",
   sample: 42,
   output: "#{opath}/Zjets",
   options: opt
  },
  {ID: "Wjets",
   sample: 43,
   output: "#{opath}/Wjets",
   options: opt
  },
  {ID: "Diboson",
   sample: 44,
   output: "#{opath}/Diboson",
   options: opt
  },
  {ID: "Triboson",
   sample: 45,
   output: "#{opath}/Triboson",
   options: opt
  },
  {ID: "ttX",
   sample: 46,
   output: "#{opath}/ttX",
   options: opt
  },
  {ID: "tW",
   sample: 47,
   output: "#{opath}/tW",
   options: opt
  },
  {ID: "tW_tau",
   sample: 48,
   output: "#{opath}/tW_tau",
   options: opt
  },
  {ID: "ttbar_tau",
   sample: 49,
   output: "#{opath}/ttbar_tau",
   options: opt
  },
  {ID: "ttbar_mu",
   sample: 50,
   output: "#{opath}/ttbar_mu",
   options: opt
  },
  {ID: "ZZ_mu",
   sample: 51,
   output: "#{opath}/ZZ_mu",
   options: opt
  },
  {ID: "Diboson_mu",
   sample: 52,
   output: "#{opath}/Diboson_mu",
   options: opt
  },
  {ID: "ttX_mu",
   sample: 53,
   output: "#{opath}/ttX_mu",
   options: opt
  },
  {ID: "Triboson_mu",
   sample: 54,
   output: "#{opath}/Triboson_mu",
   options: opt
  },
  {ID: "tZj_mu",
   sample: 55,
   output: "#{opath}/tZj_mu",
   options: opt
  },
]
#end

to_submit = {}
#to_submit["rare_first_m20"]            = " rareup_first_m20 || rarecharm_first_m20 "
#to_submit["rare_first_m50"]            = " rareup_first_m50 || rarecharm_first_m50 "
#to_submit["rare_first_m80"]            = " rareup_first_m80 || rarecharm_first_m80 "
#to_submit["rare_first_m90"]            = " rareup_first_m90 || rarecharm_first_m90 "
#to_submit["rare_first_m100"]            = " rareup_first_m100 || rarecharm_first_m100 "
#to_submit["rare_first_m120"]            = " rareup_first_m120 || rarecharm_first_m120 "
#to_submit["rare_first_m150"]            = " rareup_first_m150 || rarecharm_first_m150 "
#to_submit["rare_second_m20"]            = " rareup_second_m20 || rarecharm_second_m20 "
#to_submit["rare_second_m50"]            = " rareup_second_m50 || rarecharm_second_m50 "
#to_submit["rare_second_m80"]            = " rareup_second_m80 || rarecharm_second_m80 "
#to_submit["rare_second_m90"]            = " rareup_second_m90 || rarecharm_second_m90 "
#to_submit["rare_second_m100"]            = " rareup_second_m100 || rarecharm_second_m100 "
#to_submit["rare_second_m120"]            = " rareup_second_m120 || rarecharm_second_m120 "
#to_submit["rare_second_m150"]            = " rareup_second_m150 || rarecharm_second_m150 "
#to_submit["rare_third_m20"]            = " rareup_third_m20 || rarecharm_third_m20 "
#to_submit["rare_third_m30"]            = " rareup_third_m30 || rarecharm_third_m30 "
#to_submit["rare_third_m40"]            = " rareup_third_m40 || rarecharm_third_m40 "
to_submit["rare_third_m50"]            = " rareup_third_m50 || rarecharm_third_m50 "
#to_submit["rare_third_m60"]            = " rareup_third_m60 || rarecharm_third_m60 "
#to_submit["rare_third_m70"]            = " rareup_third_m70 || rarecharm_third_m70 "
#to_submit["rare_third_m80"]            = " rareup_third_m80 || rarecharm_third_m80 "
#to_submit["rare_third_m90"]            = " rareup_third_m90 || rarecharm_third_m90 "
#to_submit["rare_third_m100"]            = " rareup_third_m100 || rarecharm_third_m100 "
#to_submit["rare_third_m120"]            = " rareup_third_m120 || rarecharm_third_m120 "
#to_submit["rare_third_m150"]            = " rareup_third_m150 || rarecharm_third_m150 "
to_submit["Background"] = " Diboson || Diboson_mu || Triboson || Triboson_mu || Wjets || Zjets || ttX || ttX_mu || tZj || tZj_mu || tW || tW_tau || ZZ_mu || ttbar_tau || ttbar_mu "
#to_submit = {"runall" => "[a-zA-Z]+"}

# Check if run only nominal
#nominal = (ARGV.include? "nominal") ? true : false
nominal = true
to_submit.each do |k,v|
  #manager = NPA::JobManager.new(command: "RunNPA")
  manager = NPA::JobManager.new(command: "RunNPA",log: k+".log")
  if nominal
    manager.load config
  else
    manager.load config, "allsyst.json"
  end

  manager.filter v
  #manager.filter "signal"
  
  if ENV["USER"] == "apeixoto"
    manager.add_notification(NPA::Notification::Telegram.new(to:"92131189"))
  elsif ENV["USER"] == "tvale"
    manager.add_notification(NPA::Notification::Telegram.new(to:"71247486"))
  end
  #manager.run(max:10)
  psleep = ARGV.include?("test") ? 1 : 60
  manager.submit(cluster: "PBS", flags: {"-V"=> ""}, test: ARGV.include?("test"), config: PREAMBLE, sleep: psleep)
  manager.generate_xml
end
