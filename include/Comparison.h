#include "NPASampCol.h"
#include <vector>
#include <string>

// Class to compare different distributions from a xml
class Comparison{
    public:
    Comparison(std::string xml, std::string collection);
    ~Comparison();
    
    void histoAccept(std::vector<std::string> vec){m_accept = vec;};
    void histoAvoid(std::vector<std::string> vec){m_avoid = vec;};
    void compareHist(std::string hist);
    void compareAll();
    void setNorm(bool n){m_norm = n;};
    void setLog(bool n){m_log = n;};
    void setOutput(std::string s){m_output_path = s;};
    private:

    NPASampCol *m_samples;
    std::string m_output_path;
    std::vector<std::string> m_accept;
    std::vector<std::string> m_avoid;
    bool m_norm, m_log;
};
