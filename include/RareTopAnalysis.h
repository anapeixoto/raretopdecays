#ifndef RareTopAnalysis_h
#define RareTopAnalysis_h
#include "NPA.h"
#include "DelphesReader.h"
class RareTopAnalysis : public NPA {
	// Functions
	public:
	virtual void DefineSamples(); //!< Defines input files for each sample
	RareTopAnalysis (int, const char**);
	~RareTopAnalysis ();
	virtual void DoCuts(); //!< Event selection
	virtual void configuration(); //!< Define reader
	virtual void BookHistograms(); //!< Define histograms
	virtual void FillHistograms(); //!< Fill histograms
	void FirstCase(); //!< First case
	void SecondCase(); //!< Second case
	void ThirdCase(); //!< Third case
	void TestforWboson(); //!< W boson
	vector<TLorentzVector> ReconstructWboson(); //!< Fill histograms
	vector<TLorentzVector> ReconstructNu(); //!< Fill histograms
	DelphesReader *m_reader;
	double n_tautag;
	double m_S1;
	double m_S2;
	double mt_S1;
	double mt_S2;
	double pt_S1;
	double pt_S2;
	double m_W;
	double mt_W;
	double m_top;
	double mt_top;
	double m_total;
	double m_lb;
	double mt_lbMET, mt_S1MET, mt_totalMET;
	int fi, fj; //Leptons from first scalar S
	int fi2, fj2; //Leptons from second scalar S
	int wi, wj; //Jets from W boson
	TLorentzVector Wjet1, Wjet2;
	double mass_S;
	double m_top_ref, m_W_ref, cutoff_scale, top_mass_window, mass_window;
	int analysis_case; 
	TLorentzVector lep;
};
#endif
