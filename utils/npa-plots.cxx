#include <iostream>
#include <map>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <ctime>
// ROOT stuff
#include "TH1D.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TH1D.h"
#include "TLine.h"
#include "TGraphAsymmErrors.h"
#include "TStyle.h"
#include "TROOT.h"
// NPA stuff
#include "NPASampCol.h"
#include "NPAXML.h"
#include "NPASampCol.h"
#include "NPAPlotSample.h"
#include "NPATableWriter.h"
#include "Commands.h"

#include "TLatex.h"

using namespace std;
void myText(Double_t x,Double_t y,Color_t color, const char *text,float tsize) {

  TLatex l; //l.SetTextAlign(12);
  l.SetTextSize(tsize);
  l.SetTextFont(12);
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}


int doPlots(){
	if (Commands::hasKey("h")){
		cout<<R"(These are the options to run plots:
			* xml :         Mandatory, xml to be used
			* col :         Mandatory, the sample collection to be used
			* log :         Optional, use it to switch logscale 
			* lumi_label :  Optional, the luminosity label to be used 
			* cmd :         Optional, center of mass energy
			* blind :       Optional, to blind data 
			* yields :      Optional, to print yields in samples label
			* atlas:        Optional, ATLAS label (Internal, Preliminary...))"<<endl;
		return 0;
	}

	// Check for mandatory options
	string xml = Commands::must("xml",string("--")); // XML to read plot configuration
	string col = Commands::must("col",string("--")); // Sample to be plotted
	// Read optional values
	bool log =  Commands::hasKey("log");
	string lumi_label = Commands::read("lumi_label",string("150 fb^{-1}"));
	string cme = Commands::read("cme",string("13 TeV"));
	bool yields_label = Commands::hasKey("yields");

	// Define the samples collection from the xml
	// The first argument is the xml file to read
	// it is a const char so call c_str() function
	// the second is the root node, for plotting is
	// always Plots, as should be defined
	// the third is the collection we want to read
	NPASampCol samples(xml.c_str(),"Plots",col);

	//Define regions and channels for labels
        //string channel = (col.find("eemm") != string::npos) ? "ee + #mu#mu" : (col.find("ee") != string::npos) ? "ee" : "#mu#mu";	
	// Define the output path to store the plots
	// First read if it has been provided by the command line
	// and if not read it from the <Configuration> node in the xml
	// any option in the configuration node can be read with the 
	// readOption function
	string opath = Commands::read("o",string("--"));
	if (opath == "--") samples.readOptionStr("OutputDir",opath);
	system(("mkdir -p "+opath).c_str());

	// Now lets get the list of histograms to print. We can set filters
	// to avoid printing all histograms for that we read the options
	// from the json settings file
	// Default plots to write: all
	vector<string> def_accept = {"cut"};
	// Default plots to avoid: none
	vector<string> def_avoid = {};
	// If any histogram name matches any of the accept filters
	// it will be printed
	// If any histogram matchs any of the avoid filters it won't 
	// be printed, so the accpeted histograms are a combination
	// of both of them, the accepted ones which are not avoided
	vector<string> accept = Commands::readArray("plots_accept",def_accept);
	vector<string> avoid = Commands::readArray("plots_avoid",def_avoid);
	// Gt the filtered list of histograms
	samples.setHistosToAccept(accept);
	samples.setHistosToAvoid(avoid);
	vector<string> histos = samples.getListOfHistos();

	//auto plain  = new TStyle("Plain","Plain Style (no colors/fill areas)");
	//plain->SetCanvasBorderMode(0);
	//plain->SetPadBorderMode(0);
	//plain->SetPadColor(0);
	//plain->SetCanvasColor(0);
	//plain->SetTitleColor(0);
	//plain->SetOptTitle(0);
	//plain->SetStatColor(0);
	// Now we can loop over each histogram
	gStyle->SetOptTitle(0);
	int hist_num = 0;
	int hist_total = histos.size();
	for(auto &hname : histos){
		bool has_data = false;
		string cut = hname.substr(0,4);
		clock_t begin = clock();
		// Get the list of histograms from each sample
		// For that we loop over the samples adding each 
		// histogram. A sample can be a set of inputs 
		// which are the output of the analysis, as are defined in the
		// xml
		vector<TH1D*> bcks;
		vector<TH1D*> signals;
		TH1D *data;
		// Crate the vector for the legend names, these are defined
		// as the name of the sample in the xml
		vector<string> bcknames;
		vector<string> signames;
		// This while loop will end at the end of the set of 
		// samples defined in the xml
		double max = 0;
		while(NPAPlotSample *s = samples.nextSample()){
			//gROOT->SetStyle("Plain");
			TH1D *h = s->getHistogram(hname);
			// Check that the histogram exists, if not hwill be NULL
			if (h == NULL){
				cerr << "ERROR: The histogram " << hname << " does not exist on sample " << s->getName() << endl;
				return 1;
			}
			// Set the axys properly for each histogram
			// Color, line and filltype can be set in the xml
			if (max < h->GetMaximum()) max = h->GetMaximum()*10;
			h->GetXaxis()->SetLabelFont(43);
			h->GetXaxis()->SetTitleFont(43);
			h->GetXaxis()->SetLabelSize(0);
			h->GetXaxis()->SetTitleSize(0);
			h->GetYaxis()->SetLabelFont(43);
			h->GetYaxis()->SetTitleFont(43);
			h->GetYaxis()->SetTitleSize(25);
			h->GetYaxis()->SetLabelSize(25);
			//h->GetYaxis()->SetTitleOffset(2.25);
			// The type can be signal, bck and data and is defined in the xml
			if (s->getType() == "signal"){
				h->SetMarkerSize(0);
				signals.push_back(h); 
				signames.push_back(s->getName());
			}
			if (s->getType() == "bck"){
				h->SetLineColor(h->GetFillColor());
				h->SetMarkerSize(0);
				bcks.push_back(h); 
				bcknames.push_back(s->getName());
			}
			if (s->getType() == "data"){
				data = h; 
				has_data = true;
			}
		}
		// Lets reset the samples after the loop.
		// By default it ends at the end of the collection
		samples.resetSampleCounter();

		// Lets crate a THStack for the backgrounds
		THStack stack("stack","stack");
		// Add histograms for backgrounds
		// Sicne th legend and stack are filled in reverse order
		// here we need to loop from last to first
		bcks[0]->SetXTitle(bcks[0]->GetXaxis()->GetTitle());
		for ( int i = bcks.size()-1; i >= 0; i--){
			stack.Add(bcks[i]);
		}
		// The error bar is obtained as a TGraphAssymError which if requested
		// will include systematic variations
		TGraphAsymmErrors error;
		// When calling getHistogram into the entire collection
		// the histogram returned is the sum of all backgrounds
		// and th error is the backgrond error bar
		TH1D *totalbck = samples.getHistogram(hname,error);
		// Give color and fill to the total and error
		// the total wont be draw but will be used
		// for the legend for the error
		totalbck->SetFillStyle(3354);
		totalbck->SetFillColor(kBlack);
		error.SetFillStyle(3354);
		error.SetFillColor(kBlack);
		totalbck->GetXaxis()->SetLabelSize(0.045);

		//Set maximum
		if(has_data) data->SetMinimum(0.09);
		else stack.SetMinimum(1.);
		// Now lets create the canvas. It should store 2 TPads
		// one for the top pad and another one for the ratio
		TCanvas c("c1","c1",720,750);
		//c.SetBorderMode(-1);
		//c.SetBorderSize(10);
		c.SetBottomMargin(0.11);
		c.SetTopMargin(0.05);
		c.SetRightMargin(0.05);
		c.SetLeftMargin(0.13);

		totalbck->GetXaxis()->SetTitleFont(43);
		totalbck->GetYaxis()->SetLabelFont(43);
		totalbck->GetYaxis()->SetTitleFont(43);
		totalbck->SetXTitle(totalbck->GetXaxis()->GetTitle());
		totalbck->GetYaxis()->SetTitleOffset(-0.9);
		totalbck->GetXaxis()->SetTitleOffset(-0.9);
		totalbck->GetXaxis()->SetLabelSize(25);
		totalbck->GetYaxis()->SetLabelSize(25);
		totalbck->GetYaxis()->SetTitleSize(25);
		totalbck->GetXaxis()->SetTitleSize(25);
		totalbck->GetYaxis()->SetNdivisions(4);
		totalbck->SetYTitle("Events / bin");
		//c.UseCurrentStyle();
		c.Draw();
		c.cd();
		// Logscale
		if (log) c.SetLogy();
		//Set axis
		// Draw axis
		if(has_data){
			data->Draw("ex1");
			//Draw THStack
			stack.Draw("histsame");
			// Draw error bar
			error.Draw("e2same");
			// Draw signals
			for(auto &h : signals) {
				h->GetXaxis()->SetTitleFont(43);
				h->GetYaxis()->SetLabelFont(43);
				h->GetYaxis()->SetTitleFont(43);
				h->SetXTitle(totalbck->GetXaxis()->GetTitle());
				h->GetYaxis()->SetTitleOffset(2.25);
				h->GetXaxis()->SetTitleOffset(3.3);
				h->GetXaxis()->SetLabelSize(25);
				h->GetYaxis()->SetLabelSize(25);
				h->GetYaxis()->SetTitleSize(25);
				h->GetXaxis()->SetTitleSize(25);
				h->GetYaxis()->SetNdivisions(4);
				h->SetYTitle("Events / bin");
				h->Draw("histsame");
			}
			// Draw data
			data->Draw("ex1same");
		}
		else{
			stack.Draw("hist");
			// Draw error bar
			error.Draw("e2same");
			// Draw signals
			for(auto &h : signals){
				h->Draw("histsame");
			}
		}

		stack.GetXaxis()->SetLabelFont(12);
		stack.GetXaxis()->SetLabelSize(0.045);
		stack.GetXaxis()->SetTitleSize(0.055);
		stack.GetXaxis()->SetTitleFont(12);
		stack.GetXaxis()->SetTitleOffset(0.9);
		stack.GetXaxis()->SetTitle(totalbck->GetXaxis()->GetTitle());
		stack.GetYaxis()->SetTitleFont(12);
		stack.GetYaxis()->SetLabelFont(12);
		stack.GetYaxis()->SetTitleOffset(1.1);
		stack.GetYaxis()->SetLabelSize(0.045);
		stack.GetYaxis()->SetTitleSize(0.055);
		stack.GetYaxis()->SetTitle("Events / bin");
		double sigmax = -999;
		double bckmax = totalbck->GetMaximum();
		for(auto &h : signals) {
			if (h->GetMaximum() > sigmax) sigmax=h->GetMaximum();
		}
		double maxdef = sigmax > bckmax ? sigmax : bckmax;	
		stack.SetMaximum(maxdef*1.1);
		if(log) stack.SetMaximum(maxdef*1000);
		if(log) stack.SetMinimum(0.0001);
		gPad->Modified();
		gPad->Update();
		// Add legend defining height, width and offset
		float leg_height =  0.2;
		float leg_width = 0.4;
		float leg_xoffset = 0.7;
		float leg_yoffset = 0.82;
		float legX1 = 1-0.41*(596./c.GetWw())-0.08;
		float legX2 = 0.99;
		float legXmid = legX1+0.3*(legX2-legX1);
		float xi = 0., xi2 = 0., yi = 0., yi2 = 0., xf = 0., xf2 = 0., yf = 0., yf2 = 0.;
		if(!yields_label){
			xi = leg_xoffset;
			yi = leg_yoffset - leg_height/2;
			xf = leg_xoffset + leg_width;
			yf = leg_yoffset + leg_height/2;
			//TLegend leg(leg_xoffset,leg_yoffset-leg_height/2,leg_xoffset+leg_width,leg_yoffset+leg_height/2);
			//TLegend leg1(leg_xoffset + 0.1,leg_yoffset-leg_height/2,leg_xoffset+leg_width + 0.1,leg_yoffset+leg_height/2);
		}
		if(yields_label){
			xi = legX1;
			xi2 = legXmid;
			yi = 1.05 - (bcks.size()+signals.size()+2)*0.05;
			xf = legXmid;
			xf2 = legX2;
			yf = 0.98;
			//TLegend leg(legX1,1.05-(bcks.size()+signals.size()+2)*0.05, legXmid,0.98);
			//TLegend leg1(legXmid,leg.GetY1(),legX2,leg.GetY2());
		}
		TLegend leg(xi,yi,xf,yf);
		TLegend leg1(xi2,leg.GetY1(),xf2,leg.GetY2());
		// Add histograms
		leg.SetFillColor(0);
		leg.SetFillStyle(0);
		leg.SetBorderSize(0);
		leg.SetTextFont(12);
		if(yields_label){
			leg1.SetFillColor(0);
			leg1.SetFillStyle(0);
			leg1.SetBorderSize(0);
			leg1.SetTextFont(12);
		}
		if(has_data){
			leg.AddEntry(data,"Data","lep");
			if(yields_label) leg1.AddEntry((TObject*)0,Form("%.1f",data->Integral()),"");
		}
		if(yields_label){
			leg.AddEntry((TObject*)0,"Total Bkg.","");
			leg1.AddEntry((TObject*)0,Form("%.1f",totalbck->Integral()),"");
		}
		for(int i = 0; i < bcks.size(); i++){
			leg.AddEntry(bcks[i],bcknames[i].c_str(),"f");
			if(yields_label) leg1.AddEntry((TObject*)0,Form("%.1f",bcks[i]->Integral()),"");
		}
		for(int i = 0; i < signals.size(); i++){
			leg.AddEntry(signals[i],signames[i].c_str(),"l");
			if(yields_label) leg1.AddEntry((TObject*)0,Form("%.1f",signals[i]->Integral()),"");
		}
		leg.Draw("same");
		if(yields_label) leg1.Draw("same");

		//string region = (hname.find("pres") != string::npos) ? "Pre-selection" : (hname.find("dibo") != string::npos) ? "Dibosons CR" : (hname.find("ttv") != string::npos) ? "t#bar{t}V CR": (hname.find("sr") != string::npos) ? "SR" : "";
		

		std::map<std::string, string> mapOfCuts;
		mapOfCuts["cut0"]= "No cuts";
		mapOfCuts["cut1"]= "=4l";
		mapOfCuts["cut2"]= "=4l,#geq3j";
		mapOfCuts["cut3"]= "=4l,#geq3j,=1b";
		mapOfCuts["cut4"]= "=4l,#geq3j,=1b,=1t";
		mapOfCuts["cut5"]= "=4l,#geq3j,=1b,=1t";
		mapOfCuts["cut6"]= "=4l,#geq3j,=1b,=1t,=2S";
		mapOfCuts["cut7"]= "=4l,#geq3j,=1b,=1t,=2S";
		mapOfCuts["cut8"]= "=4l,#geq3j,=1b,=1t,=2S,M(tot)<1TeV";
		
		auto it = mapOfCuts.find((cut).c_str());
		// Draw labels
		// myText*x0,y0,x1,y1

		myText(0.2,0.87,1,(cme + ", "+lumi_label).c_str(),0.04);
		myText(0.2,0.83,1,"Delphes, CMS card",0.04);
		//myText(0.2,0.74,1,(cut).c_str(),0.04);
		myText(0.2,0.79,1,(it->second).c_str(),0.04);
		// Finish with the pad
		c.Modified();
		c.cd();

		// Print histogram
		string file = opath+"/"+hname;
		if (log) file += "_log";
		c.Print((file+".png").c_str());

		// Clean variables
		for (unsigned int i = 0; i < bcks.size(); i++) delete bcks[i];
		for (unsigned int i = 0; i < signals.size(); i++) delete signals[i];
		hist_num += 1;
		cout << "Percentage of sample printed: " << (float)hist_num/(float)hist_total*100 << endl;
		cout << "Elapsed seconds: " <<  double(clock() - begin) / CLOCKS_PER_SEC << endl;
	}
}

int doTables(){
	if (Commands::hasKey("h")){
		cout<<R"(These are the options to run tables:
			* xml : Mandatory, xml to be used
			* set : Mandatory, the table node to be used from the xml)"<<endl;
		return 0;
	}

	// Read options
	string xml = Commands::must("xml",string("tables.xml"));
	string set = Commands::must("set",string("default"));

	// Loop over the histograms with n_events to crate the tables
	NPATableWriter table(xml.c_str());
	int MaxCuts = 10;
	for (int i = 0; i < MaxCuts; ++i) {
		stringstream s;
		s<<"cut"<<i<<"_n_events";
		table.writeYieldsTable(s.str(),set);
	}
}

int breakdown(){
	if (Commands::hasKey("h")){
		cout<<R"(These are the options to run the systematic breakdown table:
			* xml   : Mandatory, xml to be used
			* tid   : Mandatory, table ID to be produced (as defined in the XML))"<<endl;
		return 0;
	}
	string tid = Commands::must("tid",string("---"));;
	string xml = Commands::must("xml",string("---"));;
	vector<string> regions = {"ttbar","zjets","sr0","sr1"};
	NPATableWriter table(xml.c_str());
	for (auto &r : regions) {
		stringstream s;
		s<<"cut0_"<<r<<"_n_events";
		table.writeSystTable(s.str(),tid);
	}
}

int systShape(){

	if (Commands::hasKey("h")){
		cout<<R"(These are the options to run the systematic shap plots:
			* xml   : Mandatory, xml to be used
			* col   : Mandatory, collection of samples to be used (as defined in the XML))"<<endl;
		return 0;
	}
	string xml = Commands::must("xml",string("---"));
	string col = Commands::must("col",string("---"));
	NPASampCol samples(xml.c_str(),"Plots",col);

	vector<string> def_accept = {"cut"};
	vector<string> def_avoid = {};
	vector<string> accept = Commands::readArray("plots_accept",def_accept);
	vector<string> avoid = Commands::readArray("plots_avoid",def_avoid);
	samples.setHistosToAccept(accept);
	samples.setHistosToAvoid(avoid);
	vector<string> histos = samples.getListOfHistos();
	cout<<"Running over a total of "<<histos.size()<<" histograms."<<endl;;

	for (int i = 0; i < histos.size(); i++) {
		string histoname = histos[i];
		samples.plotSystVariations(histoname.c_str());
	}
}

int doShapePlots(){
	if (Commands::hasKey("h")){
		cout<<R"(These are the options to run plots:
			* xml :         Mandatory, xml to be used
			* col :         Mandatory, the sample collection to be used
			* log :         Optional, use it to switch logscale 
			* lumi_label :  Optional, the luminosity label to be used 
			* cmd :         Optional, center of mass energy
			* atlas:        Optional, ATLAS label (Internal, Preliminary...))"<<endl;
		return 0;
	}

	// Check for mandatory options
	string xml = Commands::must("xml",string("--")); // XML to read plot configuration
	string col = Commands::must("col",string("--")); // Sample to be plotted
	// Read optional values
	bool log =  Commands::hasKey("log");
	string lumi_label = Commands::read("lumi_label",string("150 fb^{-1}"));
	string cme = Commands::read("cme",string("13 TeV"));

	NPASampCol samples(xml.c_str(),"Plots",col);

	string opath = Commands::read("o",string("--"));
	if (opath == "--") samples.readOptionStr("OutputDir",opath);
	system(("mkdir -p "+opath).c_str());

	// Get what histograms to read
	vector<string> def_accept = {"cut"};
	vector<string> def_avoid = {};
	vector<string> accept = Commands::readArray("plots_accept",def_accept);
	vector<string> avoid = Commands::readArray("plots_avoid",def_avoid);
	samples.setHistosToAccept(accept);
	samples.setHistosToAvoid(avoid);
	vector<string> histos = samples.getListOfHistos();

	for(auto &hname : histos){

		string cut = hname.substr(0,4);
		vector<TH1D*> bcks;
		vector<TH1D*> signals;
		TH1D *data;
		vector<string> bcknames;
		vector<string> signames;
		// This while loop will end at the end of the set of 
		// samples defined in the xml
		double max = 0.;
		while(NPAPlotSample *s = samples.nextSample()){
			bool has_data = false;
			TH1D *h = s->getHistogram(hname);
			// Check that the histogram exists, if not it will be NULL
			if (h == NULL){
				cerr << "ERROR: The histogram " << hname << " does not exist on sample " << s->getName() << endl;
				return 1;
			}

			// Set the axis properly for each histogram
			// Color, line and filltype can be set in the xml
			if (max < h->GetMaximum()) max = h->GetMaximum()*10;
			h->GetXaxis()->SetLabelFont(43);
			h->GetXaxis()->SetTitleFont(43);
			h->GetXaxis()->SetTitleSize(0.045);
			h->GetYaxis()->SetLabelFont(43);
			h->GetYaxis()->SetTitleFont(43);
			h->GetYaxis()->SetTitleSize(0.045);
			h->GetYaxis()->SetLabelSize(0.045);

			// The type can be signal, bck and data and is defined in the xml
			if (s->getType() == "signal"){
				h->SetMarkerSize(0);
				signals.push_back(h); 
				signames.push_back(s->getName());
			}
			if (s->getType() == "bck"){
				h->SetMarkerSize(0);
				bcks.push_back(h); 
				bcknames.push_back(s->getName());
			}
		}
		// Lets reset the samples after the loop.
		// By default it ends at the end of the collection
		samples.resetSampleCounter();

		// The error bar is obtained as a TGraphAssymError which if requested
		// will include systematic variations
		TGraphAsymmErrors error;
		// When calling getHistogram into the entire collection
		// the histogram returned is the sum of all backgrounds
		// and th error is the backgrond error bar
		TH1D *totalbck = samples.getHistogram(hname,error);

		double scale = 1./(totalbck->Integral()+totalbck->GetBinContent(0)+totalbck->GetBinContent(totalbck->GetNbinsX()+1));
		totalbck->Scale(scale);
		for(auto &h : signals) h->Scale(1./(h->Integral()+h->GetBinContent(0)+h->GetBinContent(h->GetNbinsX()+1)));
		for (int i=0; i < error.GetN(); i++) {
			error.GetY()[i] *= scale;
			error.GetEYhigh()[i] *= scale;
			error.GetEYlow()[i] *= scale;
		}
		error.SetFillStyle(3354);
		error.SetFillColor(kOrange+3);
		totalbck->GetXaxis()->SetLabelSize(0.045);
		
		double sigmax = -999;
		double bckmax = totalbck->GetMaximum();
		for(auto &h : signals) {
			if (h->GetMaximum() > sigmax) sigmax=h->GetMaximum();
		}
		double maxdef = sigmax > bckmax ? sigmax : bckmax;	

		if(log){
			totalbck->SetMaximum(maxdef*1000);
		}
		else{ 
			totalbck->SetMaximum(maxdef*1.1);
		}

		int n_bins = totalbck->GetNbinsX();
		if(hname.find("n_") != string::npos){
			totalbck->GetXaxis()->SetNdivisions(n_bins);
			for (int i = 0; i < n_bins; i++) {
				int j = (hname.find("n_lep") != string::npos) ? i : i;
				if(i == n_bins - 1) totalbck->GetXaxis()->SetBinLabel(n_bins,("#geq " + to_string(j) ).c_str());
				else totalbck->GetXaxis()->SetBinLabel(i + 1,(to_string(j)).c_str());
				totalbck->GetXaxis()->SetLabelSize(0.075);
			}
		}
		totalbck->GetYaxis()->SetTitleOffset(1.6);
		totalbck->GetYaxis()->SetTitle("Fraction of number of events");
		totalbck->SetFillColor(kOrange);
		//totalbck->SetFillStyle(3354);
		TCanvas c("c1","c1",720,750);
		//       c.SetBorderMode(-1);
		//       c.SetBorderSize(10);
		//       c.SetBottomMargin(0);
		if(log) c.SetLogy();
		c.Draw();
		c.Modified();
		c.cd();
		
		totalbck->SetStats(0);//Removes statistics box
		totalbck->Draw("hist");
		for(auto &h : signals) h->Draw("histsame");        
		gPad->RedrawAxis();
		totalbck->SetMinimum(0.0001);

		gStyle->SetLegendTextSize(0.03);
		TLegend leg(0.55,0.69,0.98,0.90);
		leg.SetFillColor(0);
		leg.SetFillStyle(0);
		leg.SetBorderSize(0);
		leg.SetTextFont(gStyle->GetTextFont());
		leg.AddEntry(totalbck,"SM Background","f");
		for(int i = 0; i < signals.size(); i++) leg.AddEntry(signals[i],signames[i].c_str(),"l");
		leg.Draw("same");

		// Draw labels
		string cme_lumi_label = cme +", "+ lumi_label;
		myText(0.19,0.855,1,cme_lumi_label.c_str(),0.035);
		//string region = (hname.find("pres") != string::npos) ? "Preselection" : (hname.find("zjets") != string::npos) ? "Z + jets CR" : (hname.find("tt") != string::npos) ? "t#bar{t} CR": (hname.find("sr") != string::npos) ? "SR" : "";
		//myText(0.19,0.81,1,(cut).c_str(),0.035);
		// Print histogram
		string file = opath+"/shape_"+hname;
		if (log) file += "_log";
		c.Print((file+".png").c_str());

		// Clean variables
		for (unsigned int i = 0; i < bcks.size(); i++) delete bcks[i];
		for (unsigned int i = 0; i < signals.size(); i++) delete signals[i];
	}
}

int main(int argc, const char *argv[])
{
	// Initialize commands
	Commands::initialize(argc,argv);

	// Read command for mode this will allow to choosse
	// what will be produced

	if (Commands::hasKey("plots")) return doPlots();
	if (Commands::hasKey("tab")) return doTables();
	if (Commands::hasKey("breakdown")) return breakdown();
	if (Commands::hasKey("systshape")) return systShape();
	if (Commands::hasKey("shapes")) return doShapePlots();

	// Print general help if no valid mode has been used
	if (Commands::hasKey("h")){
		cout << R"(Usage npa-plots [mode] [potions]
			The available modes are 
			* -plots        -> Plotting suite
			* -tab          -> Write latex tables with yields
			* -breakdown    -> Produce a breakdown of systematic uncertainties per sample
			* -systshape    -> Produce shape comparison plots between each nominal and systematic variation
			* -shapes    	-> Produce shape plots 

			To know how to use each one run npa-plots with the proper mode and the -h options
			* Ex: ./npa-plots -plots -h)"<<endl;
	} else {
		// I it gets here is because either no mode has been supplied or it is not valid
		cout << " No mode has been supplied or it has an invalid value (valid: plots, tab, comp)"<<endl;
		return 1;
	}
	return 0;
}
