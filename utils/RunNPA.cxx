#include "RareTopAnalysis.h"

using namespace std;

int main(int argc, const char *argv[])
{
    RareTopAnalysis object(argc, argv);
    object.Start(argc, argv);

    return 0;
}
