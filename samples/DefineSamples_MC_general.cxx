if (isData==0) {

    #include "../run/inputs/vars.cxx"

    // switch sample
    switch(Sample)
    {
        case 1:
            rareup_first_m20 = 1;
            break;
        case 2:
            rareup_first_m50 = 1;
            break;
        case 3:
            rareup_first_m80 = 1;
            break;
        case 56:
            rareup_first_m90 = 1;
            break;
        case 4:
            rareup_first_m100 = 1;
            break;
        case 5:
            rareup_first_m120 = 1;
            break;
        case 6:
            rareup_first_m150 = 1;
            break;
        case 7:
            rarecharm_first_m20 = 1;
            break;
        case 8:
            rarecharm_first_m50 = 1;
            break;
        case 9:
            rarecharm_first_m80 = 1;
            break;
        case 57:
            rarecharm_first_m90 = 1;
            break;
        case 10:
            rarecharm_first_m100 = 1;
            break;
        case 11:
            rarecharm_first_m120 = 1;
            break;
        case 12:
            rarecharm_first_m150 = 1;
            break;
        case 13:
            rareup_second_m20 = 1;
            break;
        case 14:
            rareup_second_m50 = 1;
            break;
        case 15:
            rareup_second_m80 = 1;
            break;
        case 58:
            rareup_second_m90 = 1;
            break;
        case 16:
            rareup_second_m100 = 1;
            break;
        case 17:
            rareup_second_m120 = 1;
            break;
        case 18:
            rareup_second_m150 = 1;
            break;
        case 19:
            rarecharm_second_m20 = 1;
            break;
        case 20:
            rarecharm_second_m50 = 1;
            break;
        case 21:
            rarecharm_second_m80 = 1;
            break;
        case 59:
            rarecharm_second_m90 = 1;
            break;
        case 22:
            rarecharm_second_m100 = 1;
            break;
        case 23:
            rarecharm_second_m120 = 1;
            break;
        case 24:
            rarecharm_second_m150 = 1;
            break;
        case 25:
            rareup_third_m20 = 1;
            break;
        case 62:
            rareup_third_m30 = 1;
            break;
        case 63:
            rareup_third_m40 = 1;
            break;
        case 26:
            rareup_third_m50 = 1;
            break;
        case 64:
            rareup_third_m60 = 1;
            break;
        case 65:
            rareup_third_m70 = 1;
            break;
        case 27:
            rareup_third_m80 = 1;
            break;
        case 60:
            rareup_third_m90 = 1;
            break;
        case 28:
            rareup_third_m100 = 1;
            break;
        case 29:
            rareup_third_m120 = 1;
            break;
        case 30:
            rareup_third_m150 = 1;
            break;
        case 31:
            rarecharm_third_m20 = 1;
            break;
        case 66:
            rarecharm_third_m30 = 1;
            break;
        case 67:
            rarecharm_third_m40 = 1;
            break;
        case 32:
            rarecharm_third_m50 = 1;
            break;
        case 68:
            rarecharm_third_m60 = 1;
            break;
        case 69:
            rarecharm_third_m70 = 1;
            break;
        case 33:
            rarecharm_third_m80 = 1;
            break;
        case 61:
            rarecharm_third_m90 = 1;
            break;
        case 34:
            rarecharm_third_m100 = 1;
            break;
        case 35:
            rarecharm_third_m120 = 1;
            break;
        case 36:
            rarecharm_third_m150 = 1;
            break;
        case 40:
            tZj = 1;
            break;
        case 41:
            ttbar = 1;
            break;
        case 42:
            Zjets = 1;
            break;
        case 43:
            Wjets = 1;
            break;
        case 44:
            Diboson = 1;
            break;
        case 45:
            Triboson = 1;
            break;
        case 46:
            ttX = 1;
            break;
        case 47:
            tW = 1;
            break;
        case 48:
            tW_tau = 1;
            break;
        case 49:
            ttbar_tau = 1;
            break;
        case 50:
            ttbar_mu = 1;
            break;
        case 51:
            ZZ_mu = 1;
            break;
        case 52:
            Diboson_mu = 1;
            break;
        case 53:
            ttX_mu = 1;
            break;
        case 54:
            Triboson_mu = 1;
            break;
        case 55:
            tZj_mu = 1;
            break;
    }
    #include "../run/inputs/activation.cxx"
}
