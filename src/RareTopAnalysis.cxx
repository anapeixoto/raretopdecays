// Automatically generated skeleton for RareTopAnalysis
#include "RareTopAnalysis.h"
#include "DelphesReader.h"
#include "Commands.h"

using namespace std;

// #############################################################################
RareTopAnalysis::RareTopAnalysis(int argc, const char *argv[]): NPA(){
// Class constructor
	if(analysis_case == 1) MaxCuts = 9;
	if(analysis_case == 2) MaxCuts = 7;
	if(analysis_case == 3) MaxCuts = 9;
}

RareTopAnalysis::~RareTopAnalysis(){
// Class destructor
}

void RareTopAnalysis::configuration(){
	TreeName = "Delphes";
	defineReader(new DelphesReader());
	m_reader = static_cast<DelphesReader*>(nTuple);
	Luminosity = Commands::read("Luminosity",3000.0);
	//Luminosity = Commands::read("Luminosity",150.0);
	analysis_case = Commands::read("case",1);
	mass_S = Commands::read("mass_S",1);
}

void RareTopAnalysis::DefineSamples(){
	// Define TMonteCarlo objects
	if (isData == 1){
		cout << "No data is being run for this analysis" << endl;
	} else if (isData == 3){
		const char *  ntu = "MiniVLQReader";
		string rootfile = "../samples/QCDntuple_BOTHmetcut_20130531.root";
		MonteCarlo.push_back(TMonteCarlo(1,999999,Luminosity,17159,"QCD", MaxCuts));
		Input.File(ntu,rootfile);
	} else {
		const char *  ntu = "DelphesReader";

		map<string,int> samples;

		string filename = "../samples/configuration.txt";
		cout<<"\tInfo in <VLQAnalysis::DefineSamples>: Reading information file: "<<filename<<endl;

#include "../samples/DefineSamples_MC_general.cxx"
		ifstream file(filename.c_str());
		string line;
		while (getline(file,line)){
			stringstream read;
			string proc;
			read<<line;
			read>>proc;
			if (samples[proc]){
				string txt;
				int type;
				int dsid;
				double wevents;
				double xsec;
				long int events;
				read >> txt >> type >> dsid >> wevents >> xsec >> events;
				cout<<"Process: "<<proc<<endl;
				cout<<"     Type:   "<<type<<endl;
				cout<<"     Dsid:   "<<dsid<<endl;
				cout<<"     Wevs:    "<<wevents<<endl;
				cout<<"     XSec:    "<<xsec<<endl;
				cout<<"     Events: "<<events<<endl;
				MonteCarlo.push_back(TMonteCarlo(type,dsid,wevents/xsec/1000, events, proc, MaxCuts));
				ifstream roots(txt.c_str());
				string rootfile;
				// ntu is defined in general.cxx
				while (getline(roots, rootfile)) Input.File(ntu,rootfile);
				roots.close();
			}
		}
		file.close();
	}
}

void RareTopAnalysis::BookHistograms(){
	// Create here all the histograms you want to be saved with something like
	addHisto("n_events",";Number of events;Events/bin",1,0.5,1.5);
	addHisto("weight",";Weight;Events/bin",200,-10,10);
	addHisto("n_jet",";Jet multiplicity;Events/bin",13,-0.5,12.5);
	addHisto("n_taujet",";Jet tau multiplicity;Events/bin",10,-0.5,9.5);
	addHisto("pt_jet",";Jet transverse momentum;Events/bin",30,0.,300.);
	addHisto("eta_jet",";Jet #eta;Events/bin",20,-5.0,5.0);
	addHisto("phi_jet",";Jet #phi;Events/bin",20,-5.0,5.0);
	addHisto("n_bjet",";B-tagged jet multiplicity;Events/bin",5,-0.5,4.5);
	addHisto("pt_bjet",";B-tagged jet transverse momentum;Events/bin",30,0.,300.);
	addHisto("eta_bjet",";B-tagged jet #eta;Events/bin",20,-5.0,5.0);
	addHisto("phi_bjet",";B-tagged jet #phi;Events/bin",20,-5.0,5.0);
	addHisto("n_lep",";Lepton multiplicity;Events/bin",10,-0.5,9.5);
	addHisto("pt_lep",";Lepton transverse momentum;Events/bin",30,0.,300.);
	addHisto("pt_lep1",";Leading lepton transverse momentum;Events/bin",30,0.,300.);
	addHisto("pt_lep2",";Sub-leading lepton transverse momentum;Events/bin",30,0.,300.);
	addHisto("eta_lep",";Lepton #eta;Events/bin",20,-5.0,5.0);
	addHisto("phi_lep",";Lepton #phi;Events/bin",20,-5.0,5.0);
	addHisto("n_el",";Electron multiplicity;Events/bin",7,-0.5,6.5);
	addHisto("pt_el",";Electron transverse momentum;Events/bin",30,0.,300.);
	addHisto("eta_el",";Electron #eta;Events/bin",20,-5.0,5.0);
	addHisto("phi_el",";Electron #phi;Events/bin",20,-5.0,5.0);
	addHisto("n_mu",";Muon multiplicity;Events/bin",6,-0.5,5.5);
	addHisto("pt_mu",";Muon transverse momentum;Events/bin",30,0.,300.);
	addHisto("eta_mu",";Muon #eta;Events/bin",20,-5.0,5.0);
	addHisto("phi_mu",";Muon #phi;Events/bin",20,-5.0,5.0);
	addHisto("MET",";Missing Transverse Energy;Events/bin",15,0.,300.);
	addHisto("m_S1",";Scalar S candidate mass;Events/bin",20,0.,200.);
	addHisto("mt_S1",";Scalar S candidate transverse mass;Events/bin",30,0.,1000.);
	addHisto("m_S2",";Scalar S candidate mass;Events/bin",16,0.,200.);
	addHisto("mt_S2",";Scalar S candidate transverse mass;Events/bin",30,0.,1000.);
	addHisto("pt_S1",";Scalar S transverse momentum;Events/bin",10,0.,300.);
	addHisto("pt_S2",";Scalar S transverse momentum;Events/bin",10,0.,300.);
	addHisto("m_lb",";Lepton and b-tagged jet system mass;Events/bin",16,0.,200.);
	addHisto("mt_lbMET",";Lepton, b-tagged jet and MET transverse mass;Events/bin",30,0.,1000.);
	addHisto("mt_S1MET",";Scalar S and MET transverse mass;Events/bin",30,0.,1000.);
	addHisto("m_W",";W boson mass;Events/bin",20,0.,500.);
	addHisto("pt_Wjet1",";Jet 1 W boson transverse mass;Events/bin",30,0.,300.);
	addHisto("pt_Wjet2",";Jet 2 W boson transverse mass;Events/bin",30,0.,300.);
	addHisto("nu_pz",";Neutrino pZ;Events/bin",40,-200.,200.);
	addHisto("m_top",";top-quark mass;Events/bin",30,0.,1000.);
	addHisto("mt_top",";top-quark transverse mass;Events/bin",30,0.,1000.);
	addHisto("m_total","; Scalar S and top-quark mass;Events/bin",30,0.,1500.);
	addHisto("mt_totalMET",";Total system (lb and S1) and MET transverse mass;Events/bin",30,0.,1000.);
}

void RareTopAnalysis::FillHistograms(){
	// In this function s whre the histograms are filled
	//fillHisto(pt.str(),LeptonVec[i].Pt()/GeV,Weight);
	//for(int i = 0; i < MaxCuts; ++i){
	fillHisto("n_events",1,Weight);
	fillHisto("weight",Weight,1);
	fillHisto("n_jet",JetVec.size(),Weight);
	fillHisto("n_taujet",n_tautag,Weight);
	for (int i = 0; i < JetVec.size(); ++i){
		fillHisto("pt_jet",JetVec[i].Pt()/GeV,Weight);
		fillHisto("eta_jet",JetVec[i].Eta(),Weight);
		fillHisto("phi_jet",JetVec[i].Phi(),Weight);
	}
	fillHisto("n_bjet",BTaggedJetVec.size(),Weight);
	for (int i = 0; i < BTaggedJetVec.size(); ++i){
		fillHisto("pt_bjet",BTaggedJetVec[i].Pt()/GeV,Weight);
		fillHisto("eta_bjet",BTaggedJetVec[i].Eta(),Weight);
		fillHisto("phi_bjet",BTaggedJetVec[i].Phi(),Weight);
	}
	fillHisto("n_lep",LeptonVec.size(),Weight);
	for (int i = 0; i < LeptonVec.size(); ++i){
		fillHisto("pt_lep",LeptonVec[i].Pt()/GeV,Weight);
		fillHisto("eta_lep",LeptonVec[i].Eta(),Weight);
		fillHisto("phi_lep",LeptonVec[i].Phi(),Weight);
	}
	if( LeptonVec.size() >= 1){
		fillHisto("pt_lep1",LeptonVec[0].Pt()/GeV,Weight);
	}
	if( LeptonVec.size() >= 2){
		fillHisto("pt_lep2",LeptonVec[1].Pt()/GeV,Weight);
	}
	fillHisto("n_el",ElectronVec.size(),Weight);
	for (int i = 0; i < ElectronVec.size(); ++i){
		fillHisto("pt_el",ElectronVec[i].Pt()/GeV,Weight);
		fillHisto("eta_el",ElectronVec[i].Eta(),Weight);
		fillHisto("phi_el",ElectronVec[i].Phi(),Weight);
	}
	fillHisto("n_mu",MuonVec.size(),Weight);
	for (int i = 0; i < MuonVec.size(); ++i){
		fillHisto("pt_mu",MuonVec[i].Pt()/GeV,Weight);
		fillHisto("eta_mu",MuonVec[i].Eta(),Weight);
		fillHisto("phi_mu",MuonVec[i].Phi(),Weight);
	}
	fillHisto("MET",MissPt,Weight);
	if ( m_S1 != 0 ){
	fillHisto("m_S1",m_S1/GeV,Weight);
	fillHisto("mt_S1",mt_S1/GeV,Weight);
	fillHisto("pt_S1",pt_S1/GeV,Weight);
	fillHisto("mt_S1MET",mt_S1MET,Weight);
	}
	if ( m_S2 != 0 ){
	fillHisto("m_S2",m_S2/GeV,Weight);
	fillHisto("mt_S2",mt_S2/GeV,Weight);
	fillHisto("pt_S2",pt_S2/GeV,Weight);
	}
	if ( m_lb != 0 ){
	fillHisto("m_lb",m_lb,Weight);
	fillHisto("mt_lbMET",mt_lbMET,Weight);
	}
	if ( m_W != 0 ){
	fillHisto("m_W",m_W,Weight);
	fillHisto("pt_Wjet1",Wjet1.Pt()/GeV,Weight);
	fillHisto("pt_Wjet2",Wjet2.Pt()/GeV,Weight);
	fillHisto("nu_pz",m_W,Weight);
	fillHisto("m_top",m_top,Weight);
	fillHisto("mt_top",mt_top,Weight);
	}
	if ( m_total != 0 ){
	fillHisto("m_total",m_total/GeV,Weight);
	}
	if ( mt_totalMET != 0 ){
	fillHisto("mt_totalMET",mt_totalMET,Weight);
	}
}

void RareTopAnalysis::DoCuts(){
	// DoCuts is used to implement cuts in the analysis. The structure is very simple:
	// if (condition to reject the event) return;
	// LastCut++;
	// All event which is not rejected needs to increase the LastCut variable.
	//for (auto& jet: JetVec) cout << jet.Pt() << endl;
	//for (auto& lep: LeptonVec) cout << lep.Pt() << endl;
	m_S1 = 0;
	mt_S1 = 0;
	pt_S1 = 0;
	mt_S1MET = 0.;
	m_S2 = 0;
	mt_S2 = 0;
	pt_S2 = 0;
	m_lb = 0.;
	mt_lbMET = 0.;
	m_W = 0;
	m_top = 0;
	mt_top = 0;
	mt_totalMET = 0.;

	//Values used for mass cuts
	m_top_ref = 172.5;
	m_W_ref = 81.2;
	cutoff_scale = 1000.;
	top_mass_window = 50.;
	mass_window = 30.;

	//Cut on the leading lepton
	if (LeptonVec.size() != 0) {
		if(LeptonVec[0].Pt()/GeV < 25) return; 
	}
	LastCut++;

	//Running one of the three cases
	if(analysis_case == 1) FirstCase();
	if(analysis_case == 2) SecondCase();
	if(analysis_case == 3) ThirdCase();

}

void RareTopAnalysis::FirstCase(){

	int klep_n = 3;
	int kjet_n = 1;
	int kbjet_n = 1;

	// Analysis for muons - First case:
	if (LeptonVec.size() != klep_n) return;
	LastCut++;
	if(JetVec.size() < kjet_n) return; 
	LastCut++;
	if(BTaggedJetVec.size() != kbjet_n) return; 
	LastCut++;
	if (MuonVec.size() < 2) return; //Needs 2 muons to reconstruct K
	//Reconstruction of the first scalar S with two muons (choosing the scalar S with the highest pT)
	int has_os_pair = -1;
	double delta = 0.;
	for (int i=0; i < MuonVec.size();i++){
		for (int j= i+1; j < MuonVec.size();j++){
			if (MuonVec[i].isb*MuonVec[j].isb > 0) continue;
			TLorentzVector fScandidate = MuonVec[i] + MuonVec[j];
			if(fScandidate.Pt() > delta){
				delta = fScandidate.Pt();
				fi = i;
				fj = j;
				has_os_pair=1;
			}
		}
	}
	if (has_os_pair != 1) return;
	TLorentzVector scalar_S1 = MuonVec[fi] + MuonVec[fj];
	if(ElectronVec.size() > 0) lep = ElectronVec[0]; //1st case: exacly 3 leptons. k-> 2muons
	else lep = MuonVec[3-fi-fj]; //Muon not in K reconstruction
	TLorentzVector fS1 = MuonVec[fi] + MuonVec[fj];
	m_S1 = fS1.M()/GeV;
	pt_S1 = fS1.Pt()/GeV;
	LastCut++;
	vector<TLorentzVector> wvec = ReconstructWboson();
	TLorentzVector fTopcandidate_First;
	if(!wvec.empty()){
		TLorentzVector fW = wvec[0]; //Only one W per event
		m_W = fW.M()/GeV;
		TLorentzVector fTopcandidate_First = fW + BTaggedJetVec[0];
		m_top = fTopcandidate_First.M()/GeV;
		LastCut++;
		if (fabs(m_top - m_top_ref) > top_mass_window) return;
	}
	LastCut++;
	TLorentzVector fTotal =  fTopcandidate_First + fS1;
	m_total = fTotal.M()/GeV;
	if (m_total > cutoff_scale) return;
	LastCut++;
	if (fabs(fS1.M() - mass_S) > mass_window) return; 
	LastCut++;
}

void RareTopAnalysis::SecondCase(){

	int klep_n = 1;
	int kjet_n = 3;
	int kbjet_n = 1;

	// Analysis for taus - Second case
	n_tautag = 0;
	if(JetVec.size() < kjet_n) return; 
	LastCut++;
	if(BTaggedJetVec.size() != kbjet_n) return; 
	LastCut++;
	if (LeptonVec.size() != 1) return;
	lep = LeptonVec[0]; //2nd case only has 1 lepton
	LastCut++;
	for (int i=0; i < JetVec.size();i++){
		if (JetVec[i].isb == 15){
			n_tautag++;
		}
	}
	if (n_tautag != 2) return; 
	int has_os_pair = -1;
	//Reconstruction of the scalar S candidate (from the requirement of exactly two tau-tagged jets, there is only one candidate)
	for (int i=0; i < JetVec.size();i++){
		for (int j=i+1; j < JetVec.size();j++){
			if (JetVec[i].isb == 15 && JetVec[j].isb == 15){
				fi = i;
				fj = j;
				has_os_pair=1;
			}
		}	
	}
	if (has_os_pair != 1) return;
	//Since we have exactly two tau-tagged jets, the has_os_pair variable is not important and it can be removed. 
	TLorentzVector fS1 = JetVec[fi] + JetVec[fj];
	m_S1 = fS1.M()/GeV;
	pt_S1 = fS1.Pt()/GeV;
	LastCut++;
	TLorentzVector flb = LeptonVec[0] + BTaggedJetVec[0];
	m_lb = flb.M()/GeV;
	TLorentzVector fTotal = flb + fS1;
	m_total = fTotal.M()/GeV;
	//Definition of the TLorentzVector of the lepton, b-tagged jet, scalar S and MET system
	mt_totalMET = TMath::Sqrt((fTotal.M()*fTotal.M())+2*(fTotal.Et()*MissPt-(fTotal.Px()*MissPx+fTotal.Py()*MissPy)));
	if (mt_totalMET > 500) return;
	LastCut++;
	if (fabs(fS1.M() - mass_S) > mass_window) return; 
	LastCut++;
}

void RareTopAnalysis::ThirdCase(){

	int klep_n = 4;
	int kjet_n = 3;
	int kbjet_n = 1;

	// Analysis for muons - Third case

	//Lepton multiplicity requirement
	if (LeptonVec.size() != klep_n) return;
	LastCut++;
	//Jet multiplicity requirement	
	if(JetVec.size() < kjet_n) return; 
	LastCut++;
	//b-tagged jet multiplicity requirement
	if(BTaggedJetVec.size() != kbjet_n) return; 
	LastCut++;
	//Reconstruction of the W boson with two non b-tagged jets	
	int has_W_can = -1;
	double delta_W = 999999999.;
	for (int i=0; i < JetVec.size();i++){
		for (int j=i+1; j < JetVec.size();j++){
			if (JetVec[i].isb != 5 && JetVec[j].isb != 5){
				TLorentzVector fWcandidate = JetVec[i] + JetVec[j];
				if(fabs(fWcandidate.M() - m_W_ref) < delta_W){
					delta_W = fabs(fWcandidate.M() - m_W_ref);
					wi = i;
					wj = j;
					has_W_can = 1;
				}
			}
		}
	}
	if (has_W_can != 1) return;
	TLorentzVector fW = JetVec[wi] + JetVec[wj];
	Wjet1 = JetVec[wi];
	Wjet2 = JetVec[wj];
	m_W=fW.M()/GeV;
	TLorentzVector fW_T;
	fW_T.SetPxPyPzE(fW.Px(),fW.Py(),0,fW.E());
	//Reconstruction of the top quark with the W boson candidate and the b-tagged jet
	TLorentzVector fTopCandidate_Third = fW + BTaggedJetVec[0];
	m_top = fTopCandidate_Third.M()/GeV;
	TLorentzVector fTopCandidate_Third_T;
	fTopCandidate_Third_T.SetPxPyPzE(fTopCandidate_Third.Px(),fTopCandidate_Third.Py(),0,fTopCandidate_Third.E());
	mt_top = fTopCandidate_Third_T.M()/GeV;
	LastCut++;
	//Cut on the top quark candidate mass 50 GeV around 175 GeV
	if (fabs(m_top - m_top_ref) > top_mass_window) return;
	LastCut++;
	//Reconstruction of the first scalar S with two muons (choosing the scalars S with the mass closer to each other)
	//Since there are exactly four muons, the possible combinations are the following: 0-1, 0-2, 0-3, 1-2, 1-3, 2-3
	//Therefore, the index for the first muon is fixed in 0, the index for the second muon varies with a for cycle and the indices for the third and fourth muon are fixed by the index of the second muon
	//The comparisons done are then: 0-1 with 2-3, 0-2 with 1-3 and 0-3 with 1-2 in order to not use the same muon for the two scalar S candidates
	int has_os_pairs = -1;
	double delta_m = 999999999.;
	fi = 0;
	int i2 = -1;
	int j2 = -1;
	for (int j=1; j < MuonVec.size();j++){
		//Choosing the muons for the second scalar with a different index from the one used for the first scalar
		vector<int> muons {1,2,3};
		muons.erase(std::remove(muons.begin(), muons.end(), j), muons.end());
		i2 = muons[0];
		j2 = muons[1];
		//Checking that the muons have opposite sign
		if (MuonVec[j].isb*MuonVec[fi].isb > 0) continue;
		if (MuonVec[i2].isb*MuonVec[j2].isb > 0) continue;
		TLorentzVector fScandidate1 = MuonVec[j] + MuonVec[fi];
		TLorentzVector fScandidate2 = MuonVec[i2] + MuonVec[j2];
		if(fabs(fScandidate1.M() - fScandidate2.M()) < delta_m){
			delta_m = fabs(fScandidate1.M() - fScandidate2.M());
			fj = j;
			fi2 = i2;
			fj2 = j2;
			has_os_pairs=1;
		}
	}
	if (has_os_pairs != 1) return;
	TLorentzVector fS1 = MuonVec[fi] + MuonVec[fj];
	m_S1=fS1.M()/GeV;
	pt_S1=fS1.Pt()/GeV;
	TLorentzVector fS2 = MuonVec[fi2] + MuonVec[fj2];
	m_S2=fS2.M()/GeV;
	pt_S2=fS2.Pt()/GeV;
	LastCut++;
	//Reconstruction of the total system (two scalars S and top quark)
	TLorentzVector fSSystem = fS1 + fS2;
	TLorentzVector fTotalSystem = fSSystem + fTopCandidate_Third;
	//Requirement that the total system mass is under 1 TeV
	m_total = fTotalSystem.M()/GeV;
	if ( m_total > cutoff_scale) return;
	LastCut++;
	//Cut on the scalars S mass 30 GeV around the considered scalar S mass
	if (fabs(fS1.M() - mass_S) > mass_window) return; 
	if (fabs(fS2.M() - mass_S) > mass_window) return; 
	LastCut++;
}

vector<TLorentzVector> RareTopAnalysis::ReconstructNu(){

	vector<TLorentzVector> nu_vec;

	//Reconstruction of the Pz of the neutrino implemented using expression (5.1) in page 59 of the following PhD thesis: http://cds.cern.ch/record/1135130?ln=en

	TLorentzVector fNucandidate;
	double EtMiss2 = (MissPx*MissPx) + (MissPy*MissPy);
	double PzMiss;

	double A0 = (81.2*81.2) / 2.;
	double A = A0 + lep.Px() * MissPx + lep.Py() * MissPy;
	double mag2 = lep.Px()*lep.Px() + lep.Py()*lep.Py() + lep.Pz()*lep.Pz();
	double perp2 = lep.Px()*lep.Px() + lep.Py()*lep.Py();
	double Delta = mag2 * (A * A - (EtMiss2 * perp2));

	if (Delta >= 0.) {
		/* two real-valued solutions: ((A * Pz(lep) +/- sqrt(Delta)) / Pt(lep)**2) -- choose the smaller one */
		double AZ = A * lep.Pz();
		PzMiss = (AZ - copysign(TMath::Sqrt(Delta), AZ)) / perp2;
		fNucandidate.SetPxPyPzE(MissPx,MissPy,PzMiss,TMath::Sqrt(EtMiss2 + (PzMiss*PzMiss)));
		nu_vec.push_back(fNucandidate);
	}
	return nu_vec;
}

vector<TLorentzVector> RareTopAnalysis::ReconstructWboson(){
	vector<TLorentzVector> w_vec;
	if(!ReconstructNu().empty()){

		TLorentzVector nu = ReconstructNu()[0]; //Only 1 neutrino in all cases. Disregarding taus???
		double wx = lep.Px() + nu.Px();
		double wy = lep.Py() + nu.Py();
		double wz = lep.Pz() + nu.Pz();
		double wE = lep.E() + nu.E();
		TLorentzVector fWcandidate;
		fWcandidate.SetPxPyPzE(wx,wy,wz,wE);
		w_vec.push_back(fWcandidate);
	}
	return w_vec;
}

