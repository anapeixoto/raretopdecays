#include <sstream>
#include <iostream>
#include "Comparison.h"
#include "TH1D.h"
#include "TLine.h"
#include "TCanvas.h"
#include "TLegend.h"

using namespace std;

Comparison::Comparison(string  xml, string collection){
    m_samples = new NPASampCol(xml.c_str(),"Plots",collection);
}

Comparison::~Comparison(){
    delete m_samples;
}

void Comparison::compareAll(){
    m_samples->setHistosToAccept(m_accept); 
    m_samples->setHistosToAvoid(m_avoid); 
    for(auto &x:m_samples->getListOfHistos()) compareHist(x);
}

void Comparison::compareHist(string hist){
    // Function that create the plot of comparison
    // The XML must have the Comparison samples defined
    // Each of the samples inside Comparison must be names as:
    //  base_0, base_1 etc. These are
    //  the samples the others are compared with.
    //
    //  Samples named as comp_0_0, comp_0_1, comp_1_0 etc. are
    //  the samples which are going to be compared with the base
    //  ones.
    //
    //  Example: 
    //      in a xml with a sample named base_0 and two samples named
    //      comp_0_ and comp_0_1 the histogram for the base and comp
    //      samples will be printed and a ratio between both comp
    //      with respect to the base will be printed as well.

    vector<TH1D*> bases;
    vector<string> leg_bases;
    vector<vector<TH1D*> > comps;
    vector<vector<string> > leg_comps;
    vector<vector<TH1D*> > ratios;

    // Fill histograms, start filling until sample is not found
    int i = 0;
    double max = 0;
    while(i >= 0){
        stringstream ss;
        ss << "base_"<<i;
        NPAPlotSample *sbase = m_samples->getSample(ss.str());
        if (sbase == nullptr) i = -1;
        else {
            leg_bases.push_back(sbase->getName());
            TH1D* h = sbase->getHistogram(hist);
            if (m_norm) h->Scale(1.0/h->Integral());
            double m = h->GetMaximum();
            if (max < m) max = m;
            h->SetLineStyle(1);
            bases.push_back(h);
            int j = 0;
            vector<TH1D*> cc;
            vector<TH1D*> ra;
            vector<string> lc;
            while (j >= 0){
                ss.str(string());
                ss << "comp_"<<i<<"_"<<j;
                NPAPlotSample *scomp = m_samples->getSample(ss.str());
                if (scomp == nullptr) j = -1;
                else {
                    lc.push_back(scomp->getName());
                    TH1D* hc = scomp->getHistogram(hist);
                    if (m_norm) hc->Scale(1.0/hc->Integral());
                    TH1D* ratio = (TH1D*)hc->Clone((scomp->getID()+"_"+hist+"_ratio").c_str());
                    ratio->Divide(h);
                    ra.push_back(ratio);
                    m = hc->GetMaximum();
                    if (max < m) max = m;
                    h->SetLineStyle(1);
                    cc.push_back(hc);
                    j++;
                }
            }
            i++;
            comps.push_back(cc);
            ratios.push_back(ra);
            leg_comps.push_back(lc);
        }
    }

    if (bases.size() == 0){
        cerr << "ERROR: No bases (samples named base_i) have been found." << endl;
        return;
    }

    float tpad_h = 150;
    int n_bases = bases.size();
    double total_h = 600+tpad_h*n_bases;
    TCanvas c1("c1","c1",800,total_h+50);
    c1.SetBorderMode(-1);
    c1.SetBorderSize(10);
    c1.SetBottomMargin(0);

    int titlesize = 20;
    int labelsize = 20;
    int titleoff = 2;

    // Start drawing on base pad
    TPad ph("ph","ph",0,(tpad_h*n_bases+50)/total_h,0.95,0.95);
    ph.SetTopMargin(0);
    ph.SetBottomMargin(0);
    ph.Draw();
    ph.cd();
    if (m_log) ph.SetLogy();
    for(int i = 0; i < n_bases; i++){
        auto b = bases[i];
        b->SetLineStyle(1);
        b->SetMaximum(max/0.7);
        b->GetXaxis()->SetLabelFont(43);
        b->GetXaxis()->SetTitleFont(43);
        b->GetXaxis()->SetLabelSize(0);
        b->GetXaxis()->SetTitleSize(0);
        b->GetYaxis()->SetLabelFont(43);
        b->GetYaxis()->SetTitleFont(43);
        b->GetYaxis()->SetTitleSize(titlesize);
        b->GetYaxis()->SetLabelSize(labelsize);
        b->GetYaxis()->SetTitleOffset(titleoff);
        if (m_norm) b->SetYTitle("Normalised entries");
        else b->SetYTitle("Entries");
        if ( i == 0 ) b->Draw("hist");
        else b->Draw("histsame");
        for (int j = 0; j < comps[i].size(); j++){
            auto &c = comps[i][j];
            c->SetLineStyle(2+j);
            c->Draw("histsame");
        }
    }
    // Add legend
    float leg_height = 0.3;
    float leg_width = 0.53;
    float leg_xoffset = 0.6;
    float leg_yoffset = 0.73;
    TLegend leg(leg_xoffset,leg_yoffset-leg_height/2,leg_xoffset+leg_width,leg_yoffset+leg_height/2);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.04);
    for(int i = 0; i < n_bases; i ++){
        leg.AddEntry(bases[i],leg_bases[i].c_str(),"l");
        for(int j = 0; j < comps[i].size(); j++) leg.AddEntry(comps[i][j],leg_comps[i][j].c_str(),"l");
    }
    leg.Draw("same");
    //ATLASLabel(0.2,0.9,"Internal");
    ph.Modified();
    c1.cd();

    // Start drawing rest of ratios histograms
    // TPad sizes are automatically derived based on a tpad_h px pad for the ratio plots
    vector<TPad*> all_pads;
    double bottomprev, topnow;
    bottomprev = ph.GetBottomMargin();
    for (int i = 0; i < n_bases; i++){
        stringstream ss;
        ss << "p_" << i;
        TPad *p;
        if ( i != n_bases-1) p = new TPad(ss.str().c_str(),ss.str().c_str(),0,(tpad_h*(n_bases-i-1)+50)/total_h,0.95,(tpad_h*(n_bases-i)+50)/total_h);
        else {
            p  = new TPad(ss.str().c_str(),ss.str().c_str(),0,tpad_h*(n_bases-i-1)/total_h,0.95,(tpad_h*(n_bases-i)+50)/total_h);
            p->SetBottomMargin(0.3);
        }
        p->SetTopMargin(0);
        bottomprev = p->GetBottomMargin();
        if (i != n_bases-1) p->SetBottomMargin(0);
        else p->SetBorderMode(-1);
        p->Draw();
        p->cd();
        for (int j = 0; j < ratios[i].size();j++){
            TH1D *h = ratios[i][j];
            h->SetLineStyle(2+j);
            h->GetXaxis()->SetTitleFont(43);
            h->GetXaxis()->SetLabelFont(43);
            h->GetXaxis()->SetTitleOffset(6);
            h->GetXaxis()->SetTitleSize(titlesize);
            h->GetXaxis()->SetLabelSize(labelsize);
            h->GetYaxis()->SetTitleFont(43);
            h->GetYaxis()->SetLabelFont(43);
            h->GetYaxis()->SetTitleSize(titlesize);
            h->GetYaxis()->SetLabelSize(labelsize);
            h->GetYaxis()->SetTitleOffset(titleoff);
            h->GetYaxis()->SetNdivisions(211);
            if ( j == 0){
                h->SetMaximum(1.59);
                h->SetMinimum(0.41);
                h->SetYTitle("Ratio");
                h->SetMarkerSize(0);
                h->Draw("e1");
            } else  h->Draw("e1same");
        }
        TLine *line = new TLine(bases[i]->GetXaxis()->GetXmin(),1.0,bases[i]->GetXaxis()->GetXmax(),1.0);
        line->SetLineStyle(2);
        line->SetLineWidth(1);
        line->Draw();
        p->Modified();
        all_pads.push_back(p);
        c1.cd();
    }

    c1.cd();
    c1.Modified();
    stringstream hname;
    hname << m_output_path << "/" << m_samples->getName()<< "_" << hist;
    if (m_norm) hname << "_norm";
    if (m_log) hname << "_log";
    hname << ".pdf";
    c1.Print(hname.str().c_str());
    for(int i = 0; i < all_pads.size(); i++) delete all_pads[i];
}
